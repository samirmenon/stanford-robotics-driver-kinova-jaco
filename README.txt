A Redis-Driver for the kinova-jaco

By the Stanford Robotics Lab

Reads-writes to a redis server and allows different control paradigms.

====================================
Getting up and running with a Kinova:

NOTE : FOLLOW THIS RELIGIOUSLY! DO NOT BE CREATIVE!
NOTE : WE DO NOT SUPPORT THIS DRIVER ON VIRTUAL MACHINES. USE A NATIVE UBUNTU INSTALL.

Step 0 : Install prerequisites

Make sure you have the basic libraries (we'll assume you have git etc.)
$ sudo apt-get install build-essential cmake libhiredis-dev libjsoncpp-dev libc6-dev-i386 redis-server redis-tools

Step 1 : Install the kinova middleware

$ sh setup.sh
(You might need permission to download the Stanford Robotics middleware version. Contact Samir Menon)

$ cd kinova-middleware.git
Follow the instructions in the readme.

Step 2 : Run the gravity estimation routine

$ cd applications-linux/jaco6-gravity-calibration/
$ sh make_rel.sh
$ ./jaco6_gravity_calibration

Wait about 10-15 minutes and steer clear of the robot. It will move around and do random stuff.

Note the set of 16 numbers it outputs.

Step 3 : Compile the driver application of choice (for now we'll assume you'll use the redis driver)

NOTE : If you're doing this for the first time, please replace the 16 numeric gravity estimatnion constants in the code with those obtained for your robot. Then proceed to compile the driver.

$ cd applications-linux/jaco6-driver-redis
$ sh make_rel.sh

Step 4 : Set up redis keys that are required for the driver to work. Note that if this doesn't work, you might need to install python and python-redis. Look for the readme file in the driver directory for instructions on how to do this.

$ python set-keys.py

Step 5 : Connect a Kinova Jaco 6-DOF robot to the USB port and run the driver

$ ./jaco6_driver

Step 5 : Write a controller to read q, dq from redis and write control torques.


