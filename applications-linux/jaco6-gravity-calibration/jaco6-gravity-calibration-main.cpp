/* This code is based on templates provided by Kinova. It has been edited
 * for use with the Stanford Robotics Lab robots.
 *
 * The edits are subject to the following license.
 */

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright Aug 2016, Stanford Robotics Lab, Stanford University
 *
 * Edited by : Samir Menon
 */

#include <kinova-api/KinovaTypes.h>
#include <kinova-api/Kinova.API.CommLayerUbuntu.h>
#include <kinova-api/Kinova.API.UsbCommandLayerUbuntu.h>

#include <iostream>
#include <dlfcn.h> // Ubuntu dynamic load
#include <unistd.h>
#include <time.h>
#include <signal.h>

#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>
#include <vector>
#include <cmath>

using namespace std;

/** This needs to be volatile to support proper exception handling. */
static volatile int glob_exit_condition = 1;

/** Function: intCtrlCHandler
 * --------------------------
 * CTRL+c signal handler that causes the main execution loop to exit
 * in a graceful manner and allow the robot to return to its home
 * joint angle configuration.
 */
void intCtrlCHandler(int sig)
{
  if (glob_exit_condition == 0){
    exit(1);
  }

  glob_exit_condition = 0;
}


int main()
{
  std::cout<<"\n Kinova gravity compensation : Will now calibrate kinova gravity params."
      << endl;

  // Install Signal Handler
  signal(SIGINT, intCtrlCHandler);

  int result;
  int programResult = 0;
  //Handle for the library's command layer.
  void * commandLayer_handle;
  //Function pointers to the functions we need
  int(*MyInitAPI)();
  int(*MyCloseAPI)();
  int(*MyGetAngularCommand)(AngularPosition &);
  int(*MyGetAngularPosition)(AngularPosition &);
  int(*MyGetDevices)(KinovaDevice devices[MAX_KINOVA_DEVICE], int &result);
  int(*MySetActiveDevice)(KinovaDevice device);
  int(*MyGetActuatorAcceleration)(AngularAcceleration &Response);
  int(*MyGetAngularVelocity)(AngularPosition &Response);
  int(*MyRunGravityZEstimationSequence)(ROBOT_TYPE type, double OptimalzParam[OPTIMAL_Z_PARAM_SIZE]);
  int(*MySwitchTrajectoryTorque)(GENERALCONTROL_TYPE);
  int(*MySetTorqueSafetyFactor)(float factor);
  int(*MySendAngularTorqueCommand)(float Command[COMMAND_SIZE]);
  int(*MySendCartesianForceCommand)(float Command[COMMAND_SIZE]);
  int(*MySetGravityVector)(float Command[3]);
  int(*MySetGravityPayload)(float Command[GRAVITY_PAYLOAD_SIZE]);
  int(*MySetGravityOptimalZParam)(float Command[GRAVITY_PARAM_SIZE]);
  int(*MySetGravityType)(GRAVITY_TYPE Type);
  int(*MyGetAngularForceGravityFree)(AngularPosition &);
  int(*MyGetCartesianForce)(CartesianPosition &);
  //We load the library (Under Windows, use the function LoadLibrary)
  commandLayer_handle = dlopen("Kinova.API.USBCommandLayerUbuntu.so",RTLD_NOW|RTLD_GLOBAL);
  MyInitAPI = (int(*)()) dlsym(commandLayer_handle, "InitAPI");
  MyCloseAPI = (int(*)()) dlsym(commandLayer_handle, "CloseAPI");
  MyGetAngularCommand = (int(*)(AngularPosition &)) dlsym(commandLayer_handle, "GetAngularCommand");
  MyGetAngularPosition = (int(*)(AngularPosition &)) dlsym(commandLayer_handle, "GetAngularPosition");
  MyGetDevices = (int(*)(KinovaDevice devices[MAX_KINOVA_DEVICE], int &result)) dlsym(commandLayer_handle, "GetDevices");
  MySetActiveDevice = (int(*)(KinovaDevice devices)) dlsym(commandLayer_handle, "SetActiveDevice");
  MyGetActuatorAcceleration = (int(*)(AngularAcceleration &)) dlsym(commandLayer_handle, "GetActuatorAcceleration");
  MyGetAngularVelocity = (int(*)(AngularPosition &)) dlsym(commandLayer_handle, "GetAngularVelocity");
  MyRunGravityZEstimationSequence = (int(*)(ROBOT_TYPE, double OptimalzParam[OPTIMAL_Z_PARAM_SIZE])) dlsym(commandLayer_handle, "RunGravityZEstimationSequence");
  MySwitchTrajectoryTorque = (int(*)(GENERALCONTROL_TYPE)) dlsym(commandLayer_handle, "SwitchTrajectoryTorque");
  MySetTorqueSafetyFactor = (int(*)(float)) dlsym(commandLayer_handle, "SetTorqueSafetyFactor");
  MySendAngularTorqueCommand = (int(*)(float Command[COMMAND_SIZE])) dlsym(commandLayer_handle, "SendAngularTorqueCommand");
  MySendCartesianForceCommand = (int(*)(float Command[COMMAND_SIZE])) dlsym(commandLayer_handle, "SendCartesianForceCommand");
  MySetGravityVector = (int(*)(float Command[3])) dlsym(commandLayer_handle, "SetGravityVector");
  MySetGravityPayload = (int(*)(float Command[GRAVITY_PAYLOAD_SIZE])) dlsym(commandLayer_handle, "SetGravityPayload");
  MySetGravityOptimalZParam = (int(*)(float Command[GRAVITY_PARAM_SIZE])) dlsym(commandLayer_handle, "SetGravityOptimalZParam");
  MySetGravityType = (int(*)(GRAVITY_TYPE Type)) dlsym(commandLayer_handle, "SetGravityType");
  MyGetAngularForceGravityFree = (int(*)(AngularPosition &)) dlsym(commandLayer_handle, "GetAngularForceGravityFree");
  MyGetCartesianForce = (int(*)(CartesianPosition &)) dlsym(commandLayer_handle, "GetCartesianForce");
  //Verify that all functions has been loaded correctly
  if ((MyInitAPI == NULL) || (MyCloseAPI == NULL) || (MyGetAngularCommand == NULL) ||
      (MySwitchTrajectoryTorque == NULL))
  {
    cout << "* * *  E R R O R   D U R I N G   I N I T I A L I Z A T I O N  * * *" << endl;
    programResult = 0;
  }
  else
  {
    cout << "I N I T I A L I Z A T I O N   C O M P L E T E D" << endl << endl;
    result = (*MyInitAPI)();
    int resultComm;
    AngularPosition DataCommand;
    // Get the angular command to test the communication with the robot
    resultComm = MyGetAngularCommand(DataCommand);
    cout << "Initialization's result :" << result << endl;
    cout << "Communication result :" << resultComm << endl;
    // If the API is initialized and the communication with the robot is working
    if (result == 1 && resultComm == 1)
    {
      // Choose robot type
      ROBOT_TYPE type = JACOV2_6DOF_SERVICE;
      double OptimalzParam[OPTIMAL_Z_PARAM_SIZE];
      // Run identification sequence
      // CAUTION READ THE FUNCTION DOCUMENTATION BEFORE
      MyRunGravityZEstimationSequence(type, OptimalzParam);

      // Now set the estimated params
      // Great Kinova! You use doubles in the function above and floats here..
      float OptimalzParamWithFloatValues[OPTIMAL_Z_PARAM_SIZE];
      for(int i=0;i<OPTIMAL_Z_PARAM_SIZE;++i){  OptimalzParamWithFloatValues[i] = OptimalzParam[i]; }
      MySetGravityOptimalZParam(OptimalzParamWithFloatValues);
      MySetGravityType(OPTIMAL);
    }
    cout << endl << "C L O S I N G   A P I" << endl;
    result = (*MyCloseAPI)();
    programResult = 1;
  }
  dlclose(commandLayer_handle);
  return programResult;
}
