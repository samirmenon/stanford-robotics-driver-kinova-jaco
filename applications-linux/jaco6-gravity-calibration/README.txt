This program uses the low level jaco communication module provided
by kinova to estimate gravity compensation parameters. Once these
are set, the gravity float mode works fairly well.
