# A script to set up the kinova redis keys.
import redis

# Connect to the redis server
r = redis.StrictRedis(host='localhost', port=6379, db=0)

# Add the robot to the set of robots
r.sadd('scl::robots', 'kinovajaco6');

# Enable torque control on the kinova
r.set('scl::robot::kinovajaco6::fgc_command_enabled', '1');

# Set some basic properties
r.set('scl::robot::kinovajaco6::dof', '6');

# Set the joint angle keys
r.set('scl::robot::kinovajaco6::sensors::q', '0 0 0 0 0 0');

# Set the joint velocity keys
r.set('scl::robot::kinovajaco6::sensors::dq', '0 0 0 0 0 0');

# Set the joint torque keys
r.set('scl::robot::kinovajaco6::actuators::fgc', '0 0 0 0 0 0');

# The operate mode
# 0 : Joint angle read
# 1 : Joint angle command
# 2 : Torque control
r.set('scl::robot::kinovajaco6::operate_mode', '2');
