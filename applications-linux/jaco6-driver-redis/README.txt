Redis driver application for the kinova jaco 6dof robot.

This program uses the low level jaco communication module provided
by kinova to read q,dq and write torques. It sends q and dq to a
redis server and obtains torques from the redis server. This allows
us to write control modules in any programming language that has a
redis api.

Runtime notes:

* You must run the set-keys script to set up the redis keys used by this driver
$ python set-keys.py

* If you don't have python, install it first
$ sudo apt-get install python python-redis

* If you don't want to install python, you may manually set the keys. Look inside the set-keys script.

********************************
IMPORTANT : Gravity compensation

This driver has gravity params estimated for a specific robot. You need to run the gravity estimation
program separtely for your specific robot. Once you've done that, you need to replace the numerical
coefficients for the gravity estimation routine in this file.


*********************
IMPORTANT : USB PORTS

It is preferable to use USB ports that are directly on the motherboard of a computer. 
I.e., do not use the front USB ports

